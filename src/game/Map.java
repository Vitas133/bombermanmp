/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.TimerTask;
import javax.swing.JOptionPane;
/**
 *
 * @author Vitas
 */
public class Map extends TimerTask {
    Player gracz = new Player();
    int x = gracz.x;
    int y = gracz.y;
    static char strona;
    int plansza[][] = new int[15][19];
    
     /**
     * Metoda odpowiadająca za wysłanie współrzędnych gracza oraz odebranie właściwej mapy z serwera, wraz z graczem.
     * @param x
     * @param y
     * @return 
     */
    public String send(int x, int y){
        String response = null;
        try{
            System.out.println("NASZE ID TO : " + Player.getID() ); 
            BufferedReader in;
            PrintWriter out;
            Socket s = new Socket( cServer.url, 9090);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);

            String login_request = "wspolrzedne:" + x + ":" + y + ":" + Player.getID();

            System.out.println(login_request);
            out.println(login_request);
            response = in.readLine();
        } catch (IOException ex) {
            if(Player.timeout == false){
            Login.saveError(ex, "Przerwano polaczenie z serwerem!");
            Player.timeout = true; 
            JOptionPane.showMessageDialog(null, "Stracono połączenie z serverem!");
            System.exit(0);
            }      
        }

        return response;
    }
    
    /**
     * Metoda odpowiadająca za wygenerowanie mapy przysłanej z serwera.
     */
    public void makeMap(){
        String stringplansza;
        stringplansza = send(x, y);
        System.out.println(stringplansza);
        String[] wsp = stringplansza.split(";");
        int[][] plansza2 = new int[wsp.length][];
        System.out.println("WSP: " + wsp.length);
        
        for (int i = 0; i < wsp.length; i++){
            String[] row = wsp[i].split(",");
            System.out.println("Kolumny: " + row.length);
            plansza2[i] = new int[row.length];
            for(int j = 0; j < row.length; j++){
                plansza2[i][j] = Integer.parseInt(row[j]);
            }
        }
        System.out.println("STRING: " + stringplansza);
        for(int i = 0; i < plansza.length; i++){
            for(int j = 0; j < plansza[0].length; j++){
                plansza[i][j] = plansza2[i][j];
            }
        }
    }
    @Override
    public void run(){
        Play.jframe.repaint();
}
    
    /**
     * Metoda odpowiadająca za poruszanie się użytkownika na planszy.
     * @param where
     */
    public void move(char where){
        switch(where){
            case 'l':
                if(plansza[x][y-1] == 0){
                   y--;
                   strona = 'l';
                }
                break;
            case 'p':
                if(plansza[x][y+1] == 0){
                   y++;
                   strona = 'p';
                }
                break;
            case 'g':
                if(plansza[x-1][y] == 0){
                   x--;
                   strona = 'g';
                }
                break;
            case 'd':
                   if(plansza[x+1][y] == 0){
                   x++;
                   strona = 'd';
                }
                break;
        }
    }
}
