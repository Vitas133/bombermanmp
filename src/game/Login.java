
package game;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Vitas
 */
public class Login extends javax.swing.JFrame implements Runnable{

    
    static String username;
    static String passwd;
    Thread timerr;
    static int ID;
    
    /**
     * Konstruktor odpowiadający za rozmieszczenie okna na środku ekranu
     */
    public Login() {
        initComponents();
        myInitComponents();
    }

private void myInitComponents(){   
    setLocationRelativeTo(null);
    start();
    pack();   
   }

    public void start(){
       if(timerr == null) timerr = new Thread(this);
       timerr.start();
    }

    /**
     * Metoda odpowiadająca za zapisywanie błędów (zmienna ex) do pliku bez komentarza użytkownika.
     * @param ex
     */
    public static void saveError(Exception ex){
    try {
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]");
        String aktualnyCzas = sdf.format(dt);
        FileWriter file = new FileWriter("errorsclient.txt", true);
        BufferedWriter plik = new BufferedWriter(file);
        plik.write(aktualnyCzas + ": " + ex);
        plik.newLine();
        plik.close();
    } catch (IOException ex1) {
        System.out.print("Nie udalo sie zapisac do pliku! ERROR: " + ex1);
    }
}

    /**
     * Przeładowana metoda odpowiadająca za zapisywanie błędów (zmienna ex) do pliku z komentarzem użytkownika (zmienna blad)
     * @param ex
     * @param blad
     */
    public static void saveError(Exception ex, String blad){
    try {
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]");
        String aktualnyCzas = sdf.format(dt);
        FileWriter file = new FileWriter("errorsclient.txt", true);
        BufferedWriter plik = new BufferedWriter(file);
        plik.write(aktualnyCzas + ": " + ex  + ", " + blad);
        plik.newLine();
        plik.close();
    } catch (IOException ex1) {
        System.out.print("Nie udalo sie zapisac do pliku! ERROR: " + ex1);
    }
}

    /**
     * Metoda zwracająca stan zalogowania danego użytkownika.
     * @return
     */
    public String connectionCheck(){
    try{
        BufferedReader in;
        PrintWriter out;
        Socket s = new Socket( cServer.url, 9090);
        in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        out = new PrintWriter(s.getOutputStream(), true);
        String passText = new String(passwordLogin.getPassword());            
        String connection_request = "connectioncheck:" + loginLogin.getText() + ":" + passText;

        System.out.println(connection_request);
        out.println(connection_request);
        String response;
        response = in.readLine();
        return response;
    }catch(Exception e){
        saveError(e);
    }
    JOptionPane.showMessageDialog(null, "Błąd połączenia z serwerem!");
    return "0";
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        passwordLogin = new javax.swing.JPasswordField();
        loginLogin = new javax.swing.JTextField();
        registerButton = new javax.swing.JLabel();
        loginButton = new javax.swing.JLabel();
        loginTextLogin = new javax.swing.JLabel();
        registerTextLogin = new javax.swing.JLabel();
        backButton = new javax.swing.JLabel();
        Zegarek = new javax.swing.JTextField();
        backgroundLogin = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 600));
        setResizable(false);
        getContentPane().setLayout(null);
        getContentPane().add(passwordLogin);
        passwordLogin.setBounds(320, 350, 130, 40);
        getContentPane().add(loginLogin);
        loginLogin.setBounds(320, 240, 130, 40);

        registerButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/registerButton.png"))); // NOI18N
        registerButton.setText("jLabel1");
        registerButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                registerButtonMousePressed(evt);
            }
        });
        getContentPane().add(registerButton);
        registerButton.setBounds(410, 440, 130, 60);

        loginButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/loginButton.png"))); // NOI18N
        loginButton.setText("jLabel1");
        loginButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                loginButtonMousePressed(evt);
            }
        });
        getContentPane().add(loginButton);
        loginButton.setBounds(240, 440, 130, 60);

        loginTextLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/Login.png"))); // NOI18N
        getContentPane().add(loginTextLogin);
        loginTextLogin.setBounds(320, 180, 130, 52);

        registerTextLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/Haslo.png"))); // NOI18N
        getContentPane().add(registerTextLogin);
        registerTextLogin.setBounds(320, 290, 130, 52);

        backButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/backButton.png"))); // NOI18N
        backButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                backButtonMousePressed(evt);
            }
        });
        getContentPane().add(backButton);
        backButton.setBounds(330, 510, 126, 54);

        Zegarek.setEditable(false);
        getContentPane().add(Zegarek);
        Zegarek.setBounds(700, 550, 90, 20);

        backgroundLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/tlo.jpg"))); // NOI18N
        getContentPane().add(backgroundLogin);
        backgroundLogin.setBounds(0, 0, 800, 600);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backButtonMousePressed
        cServer connect = new cServer();
        connect.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        connect.setVisible(true);
        dispose();
    }//GEN-LAST:event_backButtonMousePressed

    private void registerButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_registerButtonMousePressed
        try {
            BufferedReader in;
            PrintWriter out;
            Socket s = new Socket( cServer.url, 9090);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);
            
            String passText = new String(passwordLogin.getPassword());            
            String register_request = "register:" + loginLogin.getText() + ":" + passText;
            
            if( passText.equals("") || loginLogin.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Login i hasła są wymagane do założenia konta");
            }else{
                out.println(register_request);
                String response;
                try {
                    response = in.readLine();
                    if (response == null || response.equals("")) {
                        JOptionPane.showMessageDialog(null, "Pusty response");
                    }else{
                        JOptionPane.showMessageDialog(null, "Response:" + response);
                    }
                } catch (IOException ex) {
                    saveError(ex);
                }
            }
        } catch (IOException ex) {
            saveError(ex);
        }
        

            }//GEN-LAST:event_registerButtonMousePressed

    private void loginButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginButtonMousePressed
        try {
            String connected = connectionCheck();
            if(connected.equals("Login")){
                BufferedReader in;
                PrintWriter out;
                Socket s = new Socket( cServer.url, 9090);
                in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                out = new PrintWriter(s.getOutputStream(), true);

                String passText = new String(passwordLogin.getPassword());
                String login_request = "login:" + loginLogin.getText() + ":" + passText;

                if( passText.equals("") || loginLogin.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Login i hasła są wymagane do zalogowania");
                }else{
                    System.out.println(login_request);
                    out.println(login_request);
                    String response;
                    try {
                        response = in.readLine();
                        if (response == null || response.equals("")) {
                            JOptionPane.showMessageDialog(null, "Pusty response");
                        }else{
                            String[] exploded = response.split(";");
                            System.out.println(exploded[0]);
                            JOptionPane.showMessageDialog(null, "Response:" + exploded[0]);
                            if(exploded[0].equals("Login Succesfull")){
                                ID = Integer.parseInt(exploded[1]);
                                username = loginLogin.getText();
                                passwd = passText;
                                Menu menu = new Menu();
                                menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                menu.setVisible(true);
                                dispose();
                            }
                        }
                    } catch (IOException ex) {
                        saveError(ex);
                    }
                }
            }
            else if(connected.equals("Uzytkownik jest zalogowany")){
               JOptionPane.showMessageDialog(null, "Uzytkownik jest zalogowany!");
            }
        } catch (IOException ex) {
            saveError(ex);
        }


            
    }//GEN-LAST:event_loginButtonMousePressed

    static String login(){
        return username;
    }
    
    String passwd(){
        return passwd;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Zegarek;
    private javax.swing.JLabel backButton;
    private javax.swing.JLabel backgroundLogin;
    private javax.swing.JLabel loginButton;
    private javax.swing.JTextField loginLogin;
    private javax.swing.JLabel loginTextLogin;
    private javax.swing.JPasswordField passwordLogin;
    private javax.swing.JLabel registerButton;
    private javax.swing.JLabel registerTextLogin;
    // End of variables declaration//GEN-END:variables

    @Override
     public void run() {
        while (timerr == Thread.currentThread() ){
            try {
                Zegarek.setText(cServer.ee.timeNow());
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
                Login.saveError(ex);
            }
        }
    }
}
